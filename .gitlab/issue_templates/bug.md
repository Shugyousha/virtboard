## What problem did you encounter

### What is the current behaviour?

### What is the expected behaviour?

### How to reproduce

<!-- Please provide steps to reproduce the issue. If possible and relevant, please attach screenshots or videos demonstrating the unwanted behaviour. -->

### How easily reproducible is it?

<!-- Keep one: -->

Always.
More than half of the time.
Sometimes, not more than half of the time.
Occasionally.
Only happened once.

## Which version did you encounter the bug in?

<!-- Keep only the relevant one(s): -->

I Compiled it myself.

<!-- If you compiled virtboard from source, please check if a clean build on a new git checkout is affected. Please provide:

- the git revision via e.g. by running `git log -1 --pretty=oneline`
- the exact build command & config used) -->

I used the precompiled Debian package.

<!-- Please paste the output of `dpkg -s virtboard` below.-->

## What OS is affected?

<!-- Keep only the relevant one(s): -->

 - PureOS
 - Purism dev image
 - Debian testing
 - Other (which?)

## What compositor did you use?

<!-- Keep only the relevant one(s): -->

 - rootston (dev image default)
 - rootston (from other source than the dev image) <!-- Please provide details -->
 - sway (from what source?)
 - other (which?)

## What hardware are you running virtboard on?

<!-- Keep only the relevant one(s): -->

 - amd64 qemu image
 - Librem5 devkit
 - other (which?)

## Releveant logfiles

<!-- Please provide relevant logs:

- Wayland logs can be obtained by running `WAYLAND_DEBUG=1 virtboard`
- Core dumps can be found and viewed using `coredumpctl`
- Core dumps can be acquired by running `gdb path/to/virtboard`
- Logs since last boot can be read with `journalctl -b 0`
-->
