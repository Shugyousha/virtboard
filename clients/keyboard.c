/*
 * Copyright © 2012 Openismus GmbH
 * Copyright © 2012 Intel Corporation
 * Copyright (c) 2014 Jari Vetoniemi
 * Copyright (c) 2017, 2018 Drew DeVault
 * Copyright © 2018 Purism SPC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <assert.h>
#include <gio/gio.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <linux/input.h>
#include <cairo.h>
#include <systemd/sd-bus.h>

#include "keymap.h"
#include "window.h"
#include "shared/os-compatibility.h"
#include "input-method-unstable-v2-client-protocol.h"
#include "text-input-unstable-v3-client-protocol.h"
#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "virtual-keyboard-unstable-v1-client-protocol.h"
#include "shared/xalloc.h"
#include "sm.puri.OSK0.h"


struct keyboard;

struct text_input_state {
    uint32_t content_hint;
    uint32_t content_purpose;
    uint32_t change_cause;
    char *surrounding_text;
    uint32_t surrounding_cursor;
    bool active;
};

struct virtual_keyboard {
    struct zwlr_layer_shell_v1 *layer_shell;
    struct zwlr_layer_surface_v1 *layer_surface;
    struct zwp_input_method_manager_v2 *input_method_manager;
    struct zwp_input_method_v2 *input_method;
    struct zwp_virtual_keyboard_manager_v1 *keyboard_manager;
    struct zwp_virtual_keyboard_v1 *virtual_keyboard;
    struct wl_seat *seat;
    struct display *display;
    struct output *output;

    bool shell_configured;

    char *preedit_string;

    uint32_t serial;
    struct keyboard *keyboard;

    struct text_input_state pending;
    struct text_input_state current;

    char *preferred_layout;

    uint32_t buttons_held;
    bool scheduled_hidden; // the keyboard should hide as soon as buttons_held == 0
};

/// Modifiers passed to the virtual_keyboard protocol.
/// They are based on wayland's wl_keyboard, which doesn't document them.
enum key_modifier_type {
    modifier_none = 0,
    modifier_shift = 1,
    modifier_capslock = 2,
    modifier_ctrl = 4,
    modifier_altgr = 128,
};

enum key_type {
    keytype_default,
    keytype_backspace,
    keytype_enter,
    keytype_space,
    keytype_caps,
    keytype_altgr,
    keytype_tab,
    keytype_arrow_up,
    keytype_arrow_left,
    keytype_arrow_right,
    keytype_arrow_down,
    keytype_ctrl,
};

struct key {
    enum key_type key_type;

    char *label;
    char *uppercase;
    char *symbol;
    char *symbol_upper;
    unsigned keycode;

    unsigned int width;
};

struct layout {
    const struct key *keys;
    uint32_t count;

    const char *keymap_str;

    uint32_t columns;
    uint32_t rows;

    const char *language;

    bool has_symbol_upper;
};

static const struct key normal_keys[] = {
    { keytype_default, "q", "Q", "1", NULL, KEY_Q, 1},
    { keytype_default, "w", "W", "2", NULL, KEY_W, 1},
    { keytype_default, "e", "E", "3", NULL, KEY_E, 1},
    { keytype_default, "r", "R", "4", NULL, KEY_R, 1},
    { keytype_default, "t", "T", "5", NULL, KEY_T, 1},
    { keytype_default, "y", "Y", "6", NULL, KEY_Y, 1},
    { keytype_default, "u", "U", "7", NULL, KEY_U, 1},
    { keytype_default, "i", "I", "8", NULL, KEY_I, 1},
    { keytype_default, "o", "O", "9", NULL, KEY_O, 1},
    { keytype_default, "p", "P", "0", NULL, KEY_P, 1},
    { keytype_backspace, "⌫", "⌫", "⌫", NULL, 0, 2},

    { keytype_tab, "⇥", "⇥", "⇥", NULL, 0, 1},
    { keytype_default, "a", "A", "-", NULL, KEY_A, 1},
    { keytype_default, "s", "S", "@", NULL, KEY_S, 1},
    { keytype_default, "d", "D", "*", NULL, KEY_D, 1},
    { keytype_default, "f", "F", "^", NULL, KEY_F, 1},
    { keytype_default, "g", "G", ":", NULL, KEY_G, 1},
    { keytype_default, "h", "H", ";", NULL, KEY_H, 1},
    { keytype_default, "j", "J", "(", NULL, KEY_J, 1},
    { keytype_default, "k", "K", ")", NULL, KEY_K, 1},
    { keytype_default, "l", "L", "~", NULL, KEY_L, 1},
    { keytype_enter, "↵", "↵", "↵", NULL, 0, 2},

    { keytype_caps, "ABC", "abc", "ABC", NULL, 0, 2},
    { keytype_default, "z", "Z", "/", NULL, KEY_Z, 1},
    { keytype_default, "x", "X", "\'", NULL, KEY_X, 1},
    { keytype_default, "c", "C", "\"", NULL, KEY_C, 1},
    { keytype_default, "v", "V", "+", NULL, KEY_V, 1},
    { keytype_default, "b", "B", "=", NULL, KEY_B, 1},
    { keytype_default, "n", "N", "?", NULL, KEY_N, 1},
    { keytype_default, "m", "M", "!", NULL, KEY_M, 1},
    { keytype_default, ",", ",", "\\", NULL, KEY_COMMA, 1},
    { keytype_default, ".", ".", "|", NULL, KEY_DOT, 1},
    { keytype_caps, "ABC", "abc", "ABC", NULL, 0, 1},

    { keytype_altgr, "?123", "?123", "abc", NULL, 0, 1},
    { keytype_space, "", "", "", NULL, 0, 5},
    { keytype_arrow_up, "↑", "↑", "↑", NULL, 0, 1},
    { keytype_arrow_left, "←", "←", "←", NULL, 0, 1},
    { keytype_arrow_right, "→", "→", "→", NULL, 0, 1},
    { keytype_arrow_down, "↓", "↓", "↓", NULL, 0, 1},
    { keytype_ctrl, "Ctrl", "Ctrl", "Ctrl", NULL, 0, 2}
};

static const struct key polish_keys[] = {
    { keytype_default, "q", "Q", "1", "~", KEY_Q, 1},
    { keytype_default, "w", "W", "2", "+", KEY_W, 1},
    { keytype_default, "e", "E", "3", "-", KEY_E, 1},
    { keytype_default, "r", "R", "4", "=", KEY_R, 1},
    { keytype_default, "t", "T", "5", "%", KEY_T, 1},
    { keytype_default, "y", "Y", "6", "^", KEY_Y, 1},
    { keytype_default, "u", "U", "7", "&", KEY_U, 1},
    { keytype_default, "i", "I", "8", "*", KEY_I, 1},
    { keytype_default, "o", "O", "9", "[", KEY_O, 1},
    { keytype_default, "p", "P", "0", "]", KEY_P, 1},
    { keytype_backspace, "⌫", "⌫", "⌫", NULL, 0, 2},

    { keytype_tab, "⇥", "⇥", "⇥", NULL, 0, 1},
    { keytype_default, "a", "A", "ą", "Ą", KEY_A, 1},
    { keytype_default, "s", "S", "ś", "Ś", KEY_S, 1},
    { keytype_default, "d", "D", "ę", "Ę", KEY_D, 1},
    { keytype_default, "f", "F", ":", "_", KEY_F, 1},
    { keytype_default, "g", "G", ";", "'", KEY_G, 1},
    { keytype_default, "h", "H", "(", "{", KEY_H, 1},
    { keytype_default, "j", "J", ")", "}", KEY_J, 1},
    { keytype_default, "k", "K", "ó", "Ó", KEY_K, 1},
    { keytype_default, "l", "L", "ł", "Ł", KEY_L, 1},
    { keytype_enter, "↵", "↵", "↵", NULL, 0, 2},

    { keytype_caps, "ABC", "abc", "abc", NULL, 0, 2},
    { keytype_default, "z", "Z", "ż", "Ż", KEY_Z, 1},
    { keytype_default, "x", "X", "ź", "Ź", KEY_X, 1},
    { keytype_default, "c", "C", "ć", "Ć", KEY_C, 1},
    { keytype_default, "v", "V", "–", "$", KEY_V, 1},
    { keytype_default, "b", "B", "?", "€", KEY_B, 1},
    { keytype_default, "n", "N", "!", "#", KEY_N, 1},
    { keytype_default, "m", "M", "„", "»", KEY_M, 1},
    { keytype_default, ",", ",", "”", "«", KEY_COMMA, 1},
    { keytype_default, ".", ".", "@", "…", KEY_DOT, 1},
    { keytype_caps, "ABC", "abc", "abc", NULL, 0, 1},

    { keytype_altgr, "ą7?", "ą7?", "Ą%^", "ą7?", 0, 1},
    { keytype_space, "", "", "", NULL, 0, 5},
    { keytype_arrow_up, "↑", "↑", "↑", NULL, 0, 1},
    { keytype_arrow_left, "←", "←", "←", NULL, 0, 1},
    { keytype_arrow_right, "→", "→", "→", NULL, 0, 1},
    { keytype_arrow_down, "↓", "↓", "↓", NULL, 0, 1},
    { keytype_ctrl, "Ctrl", "Ctrl", "Ctrl", NULL, 0, 2}
};

static const struct key numeric_keys[] = {
    { keytype_default, "1", "1", "1", NULL, 0, 1},
    { keytype_default, "2", "2", "2", NULL, 0, 1},
    { keytype_default, "3", "3", "3", NULL, 0, 1},
    { keytype_default, "4", "4", "4", NULL, 0, 1},
    { keytype_default, "5", "5", "5", NULL, 0, 1},
    { keytype_default, "6", "6", "6", NULL, 0, 1},
    { keytype_default, "7", "7", "7", NULL, 0, 1},
    { keytype_default, "8", "8", "8", NULL, 0, 1},
    { keytype_default, "9", "9", "9", NULL, 0, 1},
    { keytype_default, "0", "0", "0", NULL, 0, 1},
    { keytype_backspace, "<--", "<--", "<--", NULL, 0, 2},

    { keytype_space, "", "", "", NULL, 0, 4},
    { keytype_enter, "Enter", "Enter", "Enter", NULL, 0, 2},
    { keytype_arrow_up, "/\\", "/\\", "/\\", NULL, 0, 1},
    { keytype_arrow_left, "<", "<", "<", NULL, 0, 1},
    { keytype_arrow_right, ">", ">", ">", NULL, 0, 1},
    { keytype_arrow_down, "\\/", "\\/", "\\/", NULL, 0, 1},
};

static const struct key arabic_keys[] = {
    { keytype_default, "ض", "ﹶ", "۱", NULL, 0,  1},
    { keytype_default, "ص", "ﹰ", "۲", NULL, 0,  1},
    { keytype_default, "ث", "ﹸ", "۳", NULL, 0,  1},
    { keytype_default, "ق", "ﹲ", "۴", NULL, 0,  1},
    { keytype_default, "ف", "ﻹ", "۵", NULL, 0,  1},
    { keytype_default, "غ", "ﺇ", "۶", NULL, 0,  1},
    { keytype_default, "ع", "`", "۷", NULL, 0,  1},
    { keytype_default, "ه", "٪", "۸", NULL, 0,  1},
    { keytype_default, "خ", ">", "۹", NULL, 0,  1},
    { keytype_default, "ح", "<", "۰", NULL, 0,  1},
    { keytype_backspace, "-->", "-->", "-->", NULL, 0, 2},

    { keytype_tab, "->|", "->|", "->|", NULL, 0, 1},
    { keytype_default, "ش", "ﹺ", "ﹼ", NULL, 0,  1},
    { keytype_default, "س", "ﹴ", "!", NULL, 0,  1},
    { keytype_default, "ي", "[", "@", NULL, 0,  1},
    { keytype_default, "ب", "]", "#", NULL, 0,  1},
    { keytype_default, "ل", "ﻷ", "$", NULL, 0,  1},
    { keytype_default, "ا", "أ", "%", NULL, 0,  1},
    { keytype_default, "ت", "-", "^", NULL, 0,  1},
    { keytype_default, "ن", "x", "&", NULL, 0, 1},
    { keytype_default, "م", "/", "*", NULL, 0,  1},
    { keytype_default, "ك", ":", "_", NULL, 0,  1},
    { keytype_default, "د", "\"", "+", NULL, 0,  1},
    { keytype_enter, "Enter", "Enter", "Enter", NULL, 0, 2},

    { keytype_caps, "Shift", "Base", "Shift", NULL, 0, 2},
    { keytype_default, "ئ", "~", ")", NULL, 0,  1},
    { keytype_default, "ء", "°", "(", NULL, 0,  1},
    { keytype_default, "ؤ", "{", "\"", NULL, 0,  1},
    { keytype_default, "ر", "}", "\'", NULL, 0,  1},
    { keytype_default, "ى", "ﺁ", "؟", NULL, 0,  1},
    { keytype_default, "ة", "'", "!", NULL, 0,  1},
    { keytype_default, "و", ",", ";", NULL, 0,  1},
    { keytype_default, "ﺯ", ".", "\\", NULL, 0,  1},
    { keytype_default, "ظ", "؟", "=", NULL, 0,  1},
    { keytype_caps, "Shift", "Base", "Shift", NULL, 0, 2},

    { keytype_altgr, "؟٣٢١", "؟٣٢١", "Base", NULL, 0, 1},
    { keytype_default, "ﻻ", "ﻵ", "|", NULL, 0,  1},
    { keytype_default, ",", "،", "،", NULL, 0, 1},
    { keytype_space, "", "", "", NULL, 0, 6},
    { keytype_default, ".", "ذ", "]", NULL, 0,  1},
    { keytype_default, "ط", "ﺝ", "[", NULL, 0,  1},
};

static const struct layout normal_layout = {
    normal_keys,
    sizeof(normal_keys) / sizeof(*normal_keys),
    keymap_normal,
    12,
    4,
    "en",
    false,
};

static const struct layout numeric_layout = {
    numeric_keys,
    sizeof(numeric_keys) / sizeof(*numeric_keys),
    NULL,
    12,
    2,
    "en",
    false,
};

static const struct layout arabic_layout = {
    arabic_keys,
    sizeof(arabic_keys) / sizeof(*arabic_keys),
    NULL,
    13,
    4,
    "ar",
    false,
};

static const struct layout polish_layout = {
    polish_keys,
    sizeof(polish_keys) / sizeof(*polish_keys),
    keymap_polish,
    12,
    4,
    "pl",
    true,
};

static const struct layout* layouts[] = {
    &normal_layout,
    &polish_layout,
    &arabic_layout,
};

static const unsigned layouts_count = sizeof(layouts) / sizeof (*layouts);

static const char* default_keymap_str = keymap_normal;

static const double key_width = 60;
static const double key_height = 50;

enum keyboard_state {
    KEYBOARD_STATE_DEFAULT,
    KEYBOARD_STATE_UPPERCASE,
    KEYBOARD_STATE_SYMBOLS,
    KEYBOARD_STATE_SYMBOLS_UPPER,
};

struct keyboard {
    struct virtual_keyboard *keyboard;
    struct window *window;
    struct widget *widget;

    enum keyboard_state state;
    bool ctrl_on;

    uint32_t scale; // output scale
    double width; // width in output units
};

struct size {
    double width;
    double height;
};

static const char* dbus_bus_name = "sm.puri.OSK0";
static const char* dbus_path_name = "/sm/puri/OSK0";

static GDBusProxy *_proxy;
static SmPuriOSK0 *dbus_interface = NULL;
static GSettings *settings = NULL;

static void __attribute__ ((format (printf, 1, 2)))
dbg(const char *fmt, ...)
{
#ifdef DEBUG
    if (!debug) {
        return;
    }
    va_list argp;

    va_start(argp, fmt);
    vfprintf(stderr, fmt, argp);
    va_end(argp);
#endif
}

static void
notify_visible(bool visible)
{
    sm_puri_osk0_set_visible(dbus_interface, visible);
}

static const char *
label_from_key(struct keyboard *keyboard,
        const struct key *key)
{
    switch(keyboard->state) {
    case KEYBOARD_STATE_DEFAULT:
        return key->label;
    case KEYBOARD_STATE_UPPERCASE:
        return key->uppercase;
    case KEYBOARD_STATE_SYMBOLS:
        return key->symbol;
    case KEYBOARD_STATE_SYMBOLS_UPPER:
        return key->symbol_upper ? key->symbol_upper : key->symbol;
    default:
        dbg("Keyboard state invalid\n");
    }

    return "";
}

static void
draw_key(struct keyboard *keyboard, const struct key *key, cairo_t *cr,
        unsigned int row, unsigned int col)
{
    const char *label;
    cairo_text_extents_t extents;

    cairo_save(cr);
    cairo_rectangle(cr,
                    col * key_width, row * key_height,
                    key->width * key_width, key_height);
    cairo_clip(cr);

    if (key->key_type == keytype_ctrl && keyboard->ctrl_on) {
        cairo_rectangle(cr,
                        col * key_width, row * key_height,
                        key->width * key_width, key_height);
        cairo_fill(cr);
        cairo_set_source_rgb(cr, 1, 1, 1);
    }

    /* Paint frame */
    cairo_rectangle(cr,
                    col * key_width, row * key_height,
                    key->width * key_width, key_height);
    cairo_set_line_width(cr, 3);
    cairo_stroke(cr);

    /* Paint text */
    label = label_from_key(keyboard, key);
    cairo_text_extents(cr, label, &extents);

    cairo_translate(cr,
                    col * key_width,
                    row * key_height);
    cairo_translate(cr,
                    (key->width * key_width - extents.width) / 2,
                    (key_height - extents.y_bearing) / 2);
    cairo_show_text(cr, label);

    cairo_restore(cr);
}

static const struct layout *
get_current_layout(struct virtual_keyboard *keyboard)
{
    if (!keyboard->current.active && keyboard->buttons_held == 0) {
        return &normal_layout;
    }

    switch (keyboard->current.content_purpose) {
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_DIGITS:
    case ZWP_TEXT_INPUT_V3_CONTENT_PURPOSE_NUMBER:
        return &numeric_layout;
    default: // TODO: iterate over a layout table instead
        if (!keyboard->preferred_layout) {
            return &normal_layout;
        } else if (strcmp(keyboard->preferred_layout, "ar") == 0) {
            return &arabic_layout;
        } else if (strcmp(keyboard->preferred_layout, "pl") == 0) {
            return &polish_layout;
        } else {
            return &normal_layout;
        }
    }
}

/* Returns size in output units */
static struct size keyboard_get_size(struct keyboard *keyboard) {
    const struct layout *layout = get_current_layout(keyboard->keyboard);
    double base_width = layout->columns * key_width / keyboard->scale;
    double base_scale = keyboard->width / base_width;
    struct size s = {
        .width = keyboard->width,
        .height = layout->rows * key_height * base_scale / keyboard->scale,
    };
    return s;
}

static void
redraw_handler(struct widget *widget, void *data)
{
    struct keyboard *keyboard = data;
    cairo_surface_t *surface;
    cairo_t *cr;
    unsigned int i;
    unsigned int row = 0, col = 0;
    const struct layout *layout = get_current_layout(keyboard->keyboard);

    surface = window_get_surface(keyboard->window);

    cr = cairo_create(surface);

    double scale = keyboard->width * keyboard->scale
            / (layout->columns * key_width);
    cairo_scale(cr, scale, scale);
    cairo_select_font_face(cr, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, 16);

    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_set_source_rgba(cr, 1, 1, 1, 0.75);
    cairo_rectangle(cr, 0, 0, layout->columns * key_width, layout->rows * key_height);
    cairo_paint(cr);

    cairo_set_operator(cr, CAIRO_OPERATOR_OVER);

    for (i = 0; i < layout->count; ++i) {
        cairo_set_source_rgb(cr, 0, 0, 0);
        draw_key(keyboard, &layout->keys[i], cr, row, col);
        col += layout->keys[i].width;
        if (col >= layout->columns) {
            row += 1;
            col = 0;
        }
    }

    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}

static void
resize_handler(struct widget *widget, int32_t width, int32_t height, void *data)
{
    struct keyboard *keyboard = data;
    zwlr_layer_surface_v1_set_size(keyboard->keyboard->layer_surface, width,
                                   height);
    zwlr_layer_surface_v1_set_exclusive_zone(keyboard->keyboard->layer_surface,
                                             height);
    wl_surface_commit(window_get_wl_surface(keyboard->window));
}

static char *
insert_text(const char *text, uint32_t offset, const char *insert)
{
    int tlen = strlen(text), ilen = strlen(insert);
    char *new_text = xmalloc(tlen + ilen + 1);

    memcpy(new_text, text, offset);
    memcpy(new_text + offset, insert, ilen);
    memcpy(new_text + offset + ilen, text + offset, tlen - offset);
    new_text[tlen + ilen] = '\0';

    return new_text;
}

static void
virtual_keyboard_commit_preedit(struct virtual_keyboard *keyboard)
{
    char *surrounding_text;

    if (!keyboard->preedit_string ||
            strlen(keyboard->preedit_string) == 0)
        return;

    zwp_input_method_v2_commit_string(keyboard->input_method,
                                      keyboard->preedit_string);
    zwp_input_method_v2_commit(keyboard->input_method, keyboard->serial);

    if (keyboard->current.surrounding_text) {
        surrounding_text = insert_text(keyboard->current.surrounding_text,
                                       keyboard->current.surrounding_cursor,
                                       keyboard->preedit_string);
        free(keyboard->current.surrounding_text);
        keyboard->current.surrounding_text = surrounding_text;
        keyboard->current.surrounding_cursor += strlen(keyboard->preedit_string);
    } else {
        keyboard->current.surrounding_text = strdup(keyboard->preedit_string);
        keyboard->current.surrounding_cursor = strlen(keyboard->preedit_string);
    }

    free(keyboard->preedit_string);
    keyboard->preedit_string = strdup("");
}

static void
virtual_keyboard_send_preedit(struct virtual_keyboard *keyboard, int32_t cursor)
{
    uint32_t index = strlen(keyboard->preedit_string);

    if (cursor > 0)
        index = cursor;
    zwp_input_method_v2_preedit_string(keyboard->input_method,
                                       keyboard->preedit_string, index, index);
    zwp_input_method_v2_commit(keyboard->input_method, keyboard->serial);
}

static const char *
prev_utf8_char(const char *s, const char *p)
{
    for (--p; p >= s; --p) {
        if ((*p & 0xc0) != 0x80)
            return p;
    }
    return NULL;
}

static void
delete_before_cursor(struct virtual_keyboard *keyboard)
{
    const char *start, *end;
    const char *surrounding_text = keyboard->current.surrounding_text;
    uint32_t surrounding_cursor = keyboard->current.surrounding_cursor;

    if (!surrounding_text) {
        dbg("delete_before_cursor: No surrounding text available\n");
        return;
    }

    start = prev_utf8_char(surrounding_text,
                           surrounding_text + surrounding_cursor);
    if (!start) {
        dbg("delete_before_cursor: No previous character to delete\n");
        return;
    }

    end = surrounding_text + surrounding_cursor;

    zwp_input_method_v2_delete_surrounding_text(keyboard->input_method,
                                                surrounding_cursor - (start - surrounding_text), 0);
    zwp_input_method_v2_commit(keyboard->input_method, keyboard->serial);

    /* Update surrounding text */
    keyboard->current.surrounding_cursor = start - surrounding_text;
    keyboard->current.surrounding_text[surrounding_cursor] = '\0';
    if (*end) {
        memmove(keyboard->current.surrounding_text
                + keyboard->current.surrounding_cursor,
                end, strlen(end));
    }
}

static char *
append(char *s1, const char *s2)
{
    int len1, len2;
    char *s;

    len1 = strlen(s1);
    len2 = strlen(s2);
    s = xrealloc(s1, len1 + len2 + 1);
    memcpy(s + len1, s2, len2);
    s[len1 + len2] = '\0';

    return s;
}

static void keyboard_update_mods(struct keyboard *keyboard) {
    unsigned mods = keyboard->ctrl_on ? modifier_ctrl : modifier_none;
    unsigned locked = 0;
    switch(keyboard->state) {
    case KEYBOARD_STATE_DEFAULT:
        break;
    case KEYBOARD_STATE_UPPERCASE:
        locked |= modifier_capslock;
        break;
    case KEYBOARD_STATE_SYMBOLS:
        mods |= modifier_altgr;
        break;
    case KEYBOARD_STATE_SYMBOLS_UPPER:
        mods |= modifier_altgr | modifier_shift;
    default:
        break;
    }
    zwp_virtual_keyboard_v1_modifiers(keyboard->keyboard->virtual_keyboard,
                                      mods, 0, locked, 0);
}

static void
try_hide(struct virtual_keyboard *keyboard);

static void
keyboard_handle_key(struct keyboard *keyboard, const struct layout *layout,
                    uint32_t time, const struct key *key,
                    struct input *input,
                    enum wl_pointer_button_state state)
{
    const char *label = label_from_key(keyboard, key);

    uint32_t key_state = (state == WL_POINTER_BUTTON_STATE_PRESSED) ? WL_KEYBOARD_KEY_STATE_PRESSED : WL_KEYBOARD_KEY_STATE_RELEASED;

    if (key_state == WL_KEYBOARD_KEY_STATE_PRESSED) {
        keyboard->keyboard->buttons_held++;
    } else {
        keyboard->keyboard->buttons_held--;
    }

    switch (key->key_type) {
    case keytype_default:
        if (!keyboard->keyboard->current.active || keyboard->ctrl_on) {
            zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                        time, key->keycode, key_state);
            break;
        }
        if (state != WL_POINTER_BUTTON_STATE_PRESSED)
            break;

        keyboard->keyboard->preedit_string =
                append(keyboard->keyboard->preedit_string,
                       label);
        virtual_keyboard_send_preedit(keyboard->keyboard, -1);
        break;
    case keytype_backspace:
        if (!keyboard->keyboard->current.active
                || (strlen(keyboard->keyboard->preedit_string) == 0
                    && strlen(keyboard->keyboard->current.surrounding_text) == 0)
                || keyboard->ctrl_on) {
            zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                        time,
                                        KEY_BACKSPACE, key_state);

            break;
        }
        if (state != WL_POINTER_BUTTON_STATE_PRESSED) {
            break;
        }
        if (strlen(keyboard->keyboard->preedit_string) == 0) {
            delete_before_cursor(keyboard->keyboard);
        } else {
            keyboard->keyboard->preedit_string[strlen(keyboard->keyboard->preedit_string) - 1] = '\0';
            virtual_keyboard_send_preedit(keyboard->keyboard, -1);
        }
        break;
    case keytype_enter:
        if (state == WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_ENTER, key_state);
        break;
    case keytype_space:
        if (!keyboard->keyboard->current.active || keyboard->ctrl_on) {
            zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                        time, KEY_SPACE, key_state);
            break;
        }

        if (state != WL_POINTER_BUTTON_STATE_PRESSED)
            break;
        keyboard->keyboard->preedit_string =
                append(keyboard->keyboard->preedit_string, " ");
        virtual_keyboard_commit_preedit(keyboard->keyboard);
        break;
    case keytype_caps:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED)
            break;
        switch(keyboard->state) {
        case KEYBOARD_STATE_DEFAULT:
            keyboard->state = KEYBOARD_STATE_UPPERCASE;
            break;
        case KEYBOARD_STATE_UPPERCASE:
            keyboard->state = KEYBOARD_STATE_DEFAULT;
            break;
        case KEYBOARD_STATE_SYMBOLS:
            keyboard->state = KEYBOARD_STATE_UPPERCASE;
            break;
        case KEYBOARD_STATE_SYMBOLS_UPPER:
            keyboard->state = KEYBOARD_STATE_DEFAULT;
            break;
        default:
            dbg("Keyboard state invalid\n");
        }
        keyboard_update_mods(keyboard);
        break;
    case keytype_altgr:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED)
            break;
        switch(keyboard->state) {
        case KEYBOARD_STATE_DEFAULT:
            keyboard->state = KEYBOARD_STATE_SYMBOLS;
            break;
        case KEYBOARD_STATE_UPPERCASE:
            keyboard->state = KEYBOARD_STATE_SYMBOLS;
            break;
        case KEYBOARD_STATE_SYMBOLS:
            if (layout->has_symbol_upper) {
                keyboard->state = KEYBOARD_STATE_SYMBOLS_UPPER;
            } else {
                keyboard->state = KEYBOARD_STATE_DEFAULT;
            }
            break;
        case KEYBOARD_STATE_SYMBOLS_UPPER:
            keyboard->state = KEYBOARD_STATE_SYMBOLS;
            break;
        default:
            dbg("Keyboard state invalid\n");
        }
        keyboard_update_mods(keyboard);
        break;
    case keytype_tab:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_TAB, key_state);
        break;
    case keytype_arrow_up:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_UP, key_state);
        break;
    case keytype_arrow_left:
        // TODO: gtk seems to handle the key press before the commit
        if (state != WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_LEFT, key_state);
        break;
    case keytype_arrow_right:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_RIGHT, key_state);
        break;
    case keytype_arrow_down:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED
                && keyboard->keyboard->current.active) {
            virtual_keyboard_commit_preedit(keyboard->keyboard);
        }
        zwp_virtual_keyboard_v1_key(keyboard->keyboard->virtual_keyboard,
                                    time, KEY_DOWN, key_state);
        break;
    case keytype_ctrl:
        if (state != WL_POINTER_BUTTON_STATE_PRESSED)
            break;
        keyboard->ctrl_on ^= true;
        keyboard_update_mods(keyboard);
        break;
    default:
        dbg("Keytype invalid\n");
    }

    if (keyboard->keyboard->scheduled_hidden
            && keyboard->keyboard->buttons_held == 0) {
        try_hide(keyboard->keyboard);
    }
}

static int
update_keymap(struct virtual_keyboard *virtual_keyboard)
{
    const char *keymap_str = default_keymap_str;
    for (unsigned i = 0; i < layouts_count; i++) {
        if (g_strcmp0(layouts[i]->language,
                virtual_keyboard->preferred_layout) == 0) {
            keymap_str = layouts[i]->keymap_str;
            break;
        }
    }

#ifdef KEYMAP_FILE
    // Testing only
    char fname[80];
    snprintf(fname, 79, "./keymap_%s",
             virtual_keyboard->preferred_layout
             ? virtual_keyboard->preferred_layout : "en");
    int ffd;
    struct stat sb;
    ffd = open(fname, O_RDONLY);
    if (ffd < 0) {
        return -1;
    }
    fstat(ffd, &sb);
    keymap_str = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, ffd, 0);
    if (!keymap_str) {
        close(ffd);
        return -1;
    }
#endif

    size_t keymap_size = strlen(keymap_str) + 1;
    int keymap_fd = os_create_anonymous_file(keymap_size);
    if (keymap_fd < 0) {
        goto err;
    }
    // TODO: save those inside layouts
    void *ptr = mmap(NULL, keymap_size, PROT_READ | PROT_WRITE, MAP_SHARED,
        keymap_fd, 0);
    if (ptr == (void*)-1) {
        goto err2;
    }
    strcpy(ptr, keymap_str);
    zwp_virtual_keyboard_v1_keymap(virtual_keyboard->virtual_keyboard,
                                   WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1,
                                   keymap_fd, keymap_size);
    return 0;
err2:
    close(keymap_fd);
err:
    return 1;
}

static void
make_virtual_keyboard(struct virtual_keyboard *virtual_keyboard)
{
    struct zwp_virtual_keyboard_v1 *vkeyboard;
    vkeyboard = zwp_virtual_keyboard_manager_v1_create_virtual_keyboard(
                virtual_keyboard->keyboard_manager,
                virtual_keyboard->seat);
    virtual_keyboard->virtual_keyboard = vkeyboard;
}


static void
button_handler(struct widget *widget, struct input *input, uint32_t time,
        uint32_t button, enum wl_pointer_button_state state, void *data)
{
    struct keyboard *keyboard = data;
    struct rectangle allocation;
    int32_t x, y;
    int row, col;
    unsigned int i;
    const struct layout *layout;

    layout = get_current_layout(keyboard->keyboard);

    if (button != BTN_LEFT) {
        return;
    }

    input_get_position(input, &x, &y);

    widget_get_allocation(keyboard->widget, &allocation);

    double scale = keyboard->width / (layout->columns * key_width);

    x /= scale;
    y /= scale;

    x -= allocation.x;
    y -= allocation.y;

    row = y / key_height;
    col = x / key_width + row * layout->columns;
    for (i = 0; i < layout->count; ++i) {
        col -= layout->keys[i].width;
        if (col < 0) {
            keyboard_handle_key(keyboard, layout,
                                time, &layout->keys[i],
                                input,
                                state);
            break;
        }
    }
    if (!keyboard->window) {
        return;
    }

    if (!keyboard->window) {
        return;
    }

    struct size size = keyboard_get_size(keyboard);
    window_schedule_resize(keyboard->window, size.width, size.height);
    widget_schedule_redraw(widget);
}

static void
touch_handler(struct input *input, uint32_t time,
         float x, float y, uint32_t state, void *data)
{
    struct keyboard *keyboard = data;
    struct rectangle allocation;
    int row, col;
    unsigned int i;
    const struct layout *layout;

    layout = get_current_layout(keyboard->keyboard);

    widget_get_allocation(keyboard->widget, &allocation);

    double scale = keyboard->width / (layout->columns * key_width);

    x /= scale;
    y /= scale;

    x -= allocation.x;
    y -= allocation.y;

    row = (int)y / key_height;
    col = (int)x / key_width + row * layout->columns;
    for (i = 0; i < layout->count; ++i) {
        col -= layout->keys[i].width;
        if (col < 0) {
            keyboard_handle_key(keyboard, layout,
                                time, &layout->keys[i],
                                input,
                                state);
            break;
        }
    }

    if (!keyboard->window) {
        return;
    }

    struct size size = keyboard_get_size(keyboard);
    window_schedule_resize(keyboard->window, size.width, size.height);
    widget_schedule_redraw(keyboard->widget);
}

static void
touch_down_handler(struct widget *widget, struct input *input,
          uint32_t serial, uint32_t time, int32_t id,
          float x, float y, void *data)
{
  touch_handler(input, time, x, y, WL_POINTER_BUTTON_STATE_PRESSED, data);
}

static void
touch_up_handler(struct widget *widget, struct input *input,
        uint32_t serial, uint32_t time, int32_t id, void *data)
{
  float x, y;

  input_get_touch(input, id, &x, &y);

  touch_handler(input, time, x, y, WL_POINTER_BUTTON_STATE_RELEASED, data);
}

static void
handle_layer_surface_configure(void *data,
        struct zwlr_layer_surface_v1 *surface,
        uint32_t serial, uint32_t width, uint32_t height) {
    zwlr_layer_surface_v1_ack_configure(surface, serial);

    struct virtual_keyboard *virtual_keyboard = data;

    window_schedule_resize(virtual_keyboard->keyboard->window, width, height);
    window_uninhibit_redraw(virtual_keyboard->keyboard->window);
    virtual_keyboard->shell_configured = true;
}

static void
handle_layer_surface_closed(void *data,
        struct zwlr_layer_surface_v1 *surface) {
}

static const struct zwlr_layer_surface_v1_listener layer_surface_listener = {
    handle_layer_surface_configure,
    handle_layer_surface_closed,
};

static struct zwlr_layer_surface_v1*
make_surface(struct output *output, struct virtual_keyboard *virtual_keyboard)
{
    struct zwlr_layer_surface_v1 *ls;
    struct keyboard *keyboard = virtual_keyboard->keyboard;

    ls = zwlr_layer_shell_v1_get_layer_surface(virtual_keyboard->layer_shell,
        window_get_wl_surface(keyboard->window),
        output_get_wl_output(output),
        ZWLR_LAYER_SHELL_V1_LAYER_TOP,
        "keyboard");
    zwlr_layer_surface_v1_set_anchor(ls,
        ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM | ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT
                                     | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT);
    zwlr_layer_surface_v1_set_margin(ls, 0, 0, 0, 0);
    zwlr_layer_surface_v1_set_keyboard_interactivity(ls, 0);
    zwlr_layer_surface_v1_add_listener(ls, &layer_surface_listener, virtual_keyboard);
    // do not commit; output scale is still unknown

    window_inhibit_redraw(keyboard->window); // window redraw causes a buffer to be attached. This must not happen before wlr_surface_layer.configure

    return ls;
}

static void
kb_window_create(struct virtual_keyboard *virtual_keyboard)
{
    struct keyboard *keyboard = virtual_keyboard->keyboard;
    keyboard->window = window_create_custom(virtual_keyboard->display);
    keyboard->widget = window_add_widget(keyboard->window, keyboard);

    window_set_title(keyboard->window, "Virtual keyboard");
    window_set_user_data(keyboard->window, keyboard);
    window_set_buffer_scale(keyboard->window, keyboard->scale);

    widget_set_redraw_handler(keyboard->widget, redraw_handler);
    widget_set_resize_handler(keyboard->widget, resize_handler);
    widget_set_button_handler(keyboard->widget, button_handler);
    widget_set_touch_down_handler(keyboard->widget, touch_down_handler);
    widget_set_touch_up_handler(keyboard->widget, touch_up_handler);

    virtual_keyboard->layer_surface =
        make_surface(display_get_output(virtual_keyboard->display),
             virtual_keyboard);
    virtual_keyboard->shell_configured = false;
    if (!isnan(keyboard->width)) { // output is known, ready to display
        struct size size = keyboard_get_size(keyboard);
        zwlr_layer_surface_v1_set_size(virtual_keyboard->layer_surface,
            size.width, size.height);
        zwlr_layer_surface_v1_set_exclusive_zone(virtual_keyboard->layer_surface,
            size.height);
        wl_surface_commit(window_get_wl_surface(keyboard->window));
    }
}

static void
kb_window_destroy(struct virtual_keyboard *virtual_keyboard)
{
    zwlr_layer_surface_v1_destroy(virtual_keyboard->layer_surface);
    virtual_keyboard->layer_surface = NULL;
    widget_destroy(virtual_keyboard->keyboard->widget);
    virtual_keyboard->keyboard->widget = NULL;
    window_destroy(virtual_keyboard->keyboard->window);
    virtual_keyboard->keyboard->window = NULL;
}

static void
make_visible(struct virtual_keyboard *keyboard) {
    if (keyboard->layer_surface) {
        return;
    }
    kb_window_create(keyboard);
    notify_visible(true);
}

static void
make_hidden(struct virtual_keyboard *keyboard) {
    if (!keyboard->layer_surface) {
        return;
    }
    kb_window_destroy(keyboard);
    notify_visible(false);
}

/** Checks for clicked buttons and hides when they are all unclicked.
 */
static void
try_hide(struct virtual_keyboard *keyboard) {
    if (keyboard->buttons_held) {
        keyboard->scheduled_hidden = true;
    } else {
        keyboard->scheduled_hidden = false;
        make_hidden(keyboard);
        return;
    }
}

static void
handle_input_method_activate(void *data, struct zwp_input_method_v2 *input_method)
{
    struct virtual_keyboard *keyboard = data;

    free(keyboard->pending.surrounding_text);
    struct text_input_state defaults = {0};
    keyboard->pending = defaults;
    free(keyboard->preedit_string);
    keyboard->preedit_string = strdup("");
    keyboard->pending.active = true;
}

static void
handle_input_method_deactivate(void *data, struct zwp_input_method_v2 *input_method)
{
    struct virtual_keyboard *keyboard = data;
    keyboard->pending.active = false;
}

static void
handle_surrounding_text(void *data, struct zwp_input_method_v2 *input_method,
            const char *text, uint32_t cursor, uint32_t anchor)
{
    struct virtual_keyboard *keyboard = data;

    free(keyboard->pending.surrounding_text);
    keyboard->pending.surrounding_text = strdup(text);

    keyboard->pending.surrounding_cursor = cursor;
}

static void
handle_content_type(void *data, struct zwp_input_method_v2 *input_method,
            uint32_t hint, uint32_t purpose)
{
    struct virtual_keyboard *keyboard = data;

    keyboard->pending.content_hint = hint;
    keyboard->pending.content_purpose = purpose;
}

static void
handle_text_change_cause(void *data,
        struct zwp_input_method_v2 *zwp_input_method_v2, uint32_t cause) {
    struct virtual_keyboard *keyboard = data;
    keyboard->pending.change_cause = cause;
}

static void
handle_commit_state(void *data, struct zwp_input_method_v2 *input_method)
{
    struct virtual_keyboard *keyboard = data;

    keyboard->serial++;

    free(keyboard->current.surrounding_text);
    keyboard->current = keyboard->pending;
    struct text_input_state defaults = {0};
    keyboard->pending = defaults;
    keyboard->pending.active = keyboard->current.active;

    if (keyboard->current.active) {
        make_visible(keyboard);
    } else {
        try_hide(keyboard);
    }
}

static void
handle_unavailable(void *data, struct zwp_input_method_v2 *zwp_input_method_v2)
{
    struct virtual_keyboard *keyboard = data;
    assert(keyboard->input_method == zwp_input_method_v2);
    // no need to care about proper double-buffering, the keyboard is already decommissioned
    keyboard->current.active = false;
    zwp_input_method_v2_destroy(keyboard->input_method);
    keyboard->input_method = NULL;
    try_hide(keyboard);
}

static const struct zwp_input_method_v2_listener input_method_listener = {
    .activate = handle_input_method_activate,
    .deactivate = handle_input_method_deactivate,
    .surrounding_text = handle_surrounding_text,
    .text_change_cause = handle_text_change_cause,
    .content_type = handle_content_type,
    .done = handle_commit_state,
    .unavailable = handle_unavailable,
};

static void
make_input_method(struct virtual_keyboard *virtual_keyboard) {
    virtual_keyboard->input_method =
        zwp_input_method_manager_v2_get_input_method(
            virtual_keyboard->input_method_manager, virtual_keyboard->seat);
    zwp_input_method_v2_add_listener(virtual_keyboard->input_method,
        &input_method_listener, virtual_keyboard);
}

static void
global_handler(struct display *display, uint32_t name,
        const char *interface, uint32_t version, void *data)
{
    struct virtual_keyboard *keyboard = data;

    if (!strcmp(interface, "zwlr_layer_shell_v1")) {
        keyboard->layer_shell =
            display_bind(display, name, &zwlr_layer_shell_v1_interface, 1);
    } else if (!strcmp(interface, "zwp_input_method_manager_v2")) {
        keyboard->input_method_manager = display_bind(display, name,
            &zwp_input_method_manager_v2_interface, 1);
    } else if (!strcmp(interface, "zwp_virtual_keyboard_manager_v1")) {
        keyboard->keyboard_manager = display_bind(display, name,
            &zwp_virtual_keyboard_manager_v1_interface, 1);
    }
}

static void
keyboard_create(struct virtual_keyboard *virtual_keyboard)
{
    struct keyboard *keyboard;

    keyboard = xzalloc(sizeof *keyboard);
    keyboard->scale = 0;
    keyboard->width = nanf("");
    keyboard->keyboard = virtual_keyboard;
    virtual_keyboard->keyboard = keyboard;
}

static void
handle_output_changed(struct output *output, void *data)
{
    struct virtual_keyboard *virtual_keyboard = data;
    struct output *kb_output = display_get_output(virtual_keyboard->display);
    if (output != kb_output) {
        return;
    }
    struct keyboard *keyboard = virtual_keyboard->keyboard;
    keyboard->scale = output_get_scale(output);

    struct rectangle allocation;
    output_get_allocation(output, &allocation);
    keyboard->width = allocation.width / keyboard->scale;
    dbg("New output: %d %d\n", allocation.width / keyboard->scale,
        allocation.height / keyboard->scale);

    if (!keyboard->window) {
        return;
    }

    window_set_buffer_scale(keyboard->window, keyboard->scale);
    dbg("New scale: %d\n", keyboard->scale);

    struct zwlr_layer_surface_v1 *ls = virtual_keyboard->layer_surface;

    struct size size = keyboard_get_size(keyboard);
    zwlr_layer_surface_v1_set_size(ls, size.width, size.height);
    zwlr_layer_surface_v1_set_exclusive_zone(ls, size.height);
    wl_surface_commit(window_get_wl_surface(keyboard->window));
}

static void
session_register(void) {
    char *autostart_id = getenv("DESKTOP_AUTOSTART_ID");
    if (!autostart_id) {
        dbg("No autostart id");
        autostart_id = "";
    }
    GError *error = NULL;
    _proxy = g_dbus_proxy_new_for_bus_sync(G_BUS_TYPE_SESSION,
        G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START, NULL,
        "org.gnome.SessionManager", "/org/gnome/SessionManager",
        "org.gnome.SessionManager", NULL, &error);
    if (error) {
        fprintf(stderr, "Could not connect to session manager: %s\n",
                error->message);
        g_clear_error(&error);
        return;
    }
    // Synchronous call was *much* easier to use before glib main loop. No need to change this though.
    g_dbus_proxy_call_sync(_proxy, "RegisterClient",
        g_variant_new("(ss)", "sm.puri.Virtboard", autostart_id),
        G_DBUS_CALL_FLAGS_NONE, 1000, NULL, &error);
    if (error) {
        fprintf(stderr, "Could not register to session manager: %s\n",
                error->message);
        g_clear_error(&error);
        return;
    }
}

static gboolean
handle_get_visible(SmPuriOSK0 *object, GDBusMethodInvocation *invocation,
                   gpointer user_data)
{
    sm_puri_osk0_complete_get_visible(object, invocation,
                                      sm_puri_osk0_get_visible(object));
    return TRUE;
}

static gboolean
handle_set_visible(SmPuriOSK0 *object, GDBusMethodInvocation *invocation,
                   gboolean arg_visible, gpointer user_data)
{
    struct virtual_keyboard *keyboard = user_data;
    dbg("Received set message from dbus: %d\n", arg_visible);

    if (arg_visible) {
        make_visible(keyboard);
    } else {
        try_hide(keyboard);
    }
    sm_puri_osk0_complete_set_visible(object, invocation);
    return TRUE;
}

static void
bus_acquired(GDBusConnection *connection,
             const gchar *name,
             gpointer user_data)
{
    // dbus docs say here the object should be registered via g_dbus_connection_register_object
}

static void
bus_name_acquired(GDBusConnection *connection,
                  const gchar *name,
                  gpointer user_data)
{
    // but let's follow minminbus for now and register objects here
    dbus_interface = sm_puri_osk0_skeleton_new();

    sm_puri_osk0_set_visible(dbus_interface, false); // TODO: use actual value

    g_signal_connect(dbus_interface, "handle-get-visible",
                     G_CALLBACK(handle_get_visible), user_data);
    g_signal_connect(dbus_interface, "handle-set-visible",
                     G_CALLBACK(handle_set_visible), user_data);

    GError *error = NULL;
    if (!g_dbus_interface_skeleton_export(G_DBUS_INTERFACE_SKELETON(dbus_interface),
                                          connection,
                                          dbus_path_name,
                                          &error)) {
        fprintf(stderr, "Error registering dbus object: %s\n", error->message);
        g_clear_error(&error);
        exit(1);
    }
}

static int
dbus_osk_setup(struct virtual_keyboard *virtual_keyboard)
{
    g_bus_own_name(G_BUS_TYPE_SESSION,
                   dbus_bus_name,
                   G_BUS_NAME_OWNER_FLAGS_NONE,
                   bus_acquired, bus_name_acquired, NULL,
                   virtual_keyboard,
                   NULL);
    return 0;
}

static void
settings_set_layout(struct virtual_keyboard *virtual_keyboard)
{
    GVariant *inputs = g_settings_get_value(settings, "sources");
    guint32 index;
    g_settings_get(settings, "current", "u", &index);

    GVariantIter *iter;
    g_variant_get(inputs, "a(ss)", &iter);

    g_autofree char *type, *layout;

    bool found = false;
    for (unsigned i = 0;
         g_variant_iter_loop(iter, "(ss)", &type, &layout);
         i++) {
       if (i == index) {
            dbg("Found layout %s %s\n", type, layout);
            found = true;
            break;
        }
    }
    g_variant_iter_free(iter);
    if (found) {
        virtual_keyboard->preferred_layout = strdup(layout);
    }
}

static gboolean
settings_handle_layout_changed(GSettings *s,
                               gpointer keys, gint n_keys,
                               gpointer user_data)
{
    struct virtual_keyboard *keyboard = user_data;
    settings_set_layout(keyboard);
    update_keymap(keyboard);
    if (!keyboard->keyboard->window) {
        return TRUE;
    }

    struct size size = keyboard_get_size(keyboard->keyboard);
    window_schedule_resize(keyboard->keyboard->window, size.width, size.height);
    widget_schedule_redraw(keyboard->keyboard->widget);
    return TRUE;
}

static int
settings_listen(struct virtual_keyboard *virtual_keyboard)
{
    settings = g_settings_new("org.gnome.desktop.input-sources");
    gulong conn_id = g_signal_connect(settings, "change-event",
                                      G_CALLBACK(settings_handle_layout_changed),
                                      virtual_keyboard) == 0;
    settings_set_layout(virtual_keyboard);
    update_keymap(virtual_keyboard);
    return conn_id;
}

int
main(int argc, char *argv[])
{
#ifdef DEBUG
    const char *debugenv = getenv("VB_DEBUG");
    debug = debugenv && strcmp("1", debugenv) == 0;
#endif

    struct virtual_keyboard virtual_keyboard;

    memset(&virtual_keyboard, 0, sizeof virtual_keyboard);

    virtual_keyboard.display = display_create(&argc, argv);
    if (virtual_keyboard.display == NULL) {
        fprintf(stderr, "failed to create display: %m\n");
        return -1;
    }

    display_set_user_data(virtual_keyboard.display, &virtual_keyboard);
    display_set_global_handler(virtual_keyboard.display, global_handler);

    virtual_keyboard.seat = display_get_seat(virtual_keyboard.display);

    if (!virtual_keyboard.seat) {
        fprintf(stderr, "No seat available\n");
        return -1;
    }

    if (virtual_keyboard.layer_shell == NULL) {
        fprintf(stderr, "No layer shell global\n");
        return -1;
    }

    display_set_output_configure_handler(virtual_keyboard.display,
                                         handle_output_changed);

    if (virtual_keyboard.input_method_manager == NULL) {
        fprintf(stderr, "No input method manager global, some functionality"
                        " unavailable\n");
    } else {
        make_input_method(&virtual_keyboard);
    }
    make_virtual_keyboard(&virtual_keyboard);
    if (settings_listen(&virtual_keyboard) != 0) {
        fprintf(stderr, "Could not connect to gsettings updates, layout"
                        " changing unavailable\n");
    }
    keyboard_create(&virtual_keyboard);

    if (dbus_osk_setup(&virtual_keyboard) != 0) {
        fprintf(stderr, "Could not register dbus service\n");
        return -1;
    }

    session_register();

    display_run(virtual_keyboard.display);


    g_object_unref(settings);
    return 0;
}
