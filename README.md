Virtboard
=========

Virtboard is an on screen keyboard based on wayland-keyboard, and serving as a test bed for new input method protocols.

Currently supports:

- virtual-keyboard as proposed in [v2 email](https://lists.freedesktop.org/archives/wayland-devel/2018-May/038266.html)

Planned support:

- input-method-v2 (TBA)

Installation
------------

### Requirements

```
apt-get install meson pkg-config libpixman-1-dev libpng-dev libxcb1-dev libxcb-xkb-dev libxkbcommon-dev libcairo2-dev libwayland-dev wayland-protocols
```

### Building

```
meson . _build
```

Usage
-----

To use this version of virtboard, a compositor supporting the `virtual-keyboard-v1` protocol is required, e.g. [rootston](https://github.com/swaywm/wlroots).

Virtboard makes use of protocols that may be considered privileged by the compositor authors. For compositor-specific instructions relating to input protocols, check compositor's manual.

### rootston

The following instructions describe running virtboard with the PureOS fork of rootston from git revision [22cb6d29](https://source.puri.sm/Librem5/wlroots/commit/22cb6d293e415a0cfb1230da83d3c1b0517b102b).

Run rootston:

```
path/to/rootston/rootston
```

and leave it running.

Run virtboard:

```
$BUILD/virtboard
```

If it exits with a message like "No layer shell global", you might have another Wayland compositor running. Look at Rootston output and find a line similar to

```
2018-07-02 12:06:29 - [wlroots/rootston/main.c:45] Running compositor on wayland display 'wayland-1'
```

Remember the quoted display identifier (here it's `wayland-1`) and run virtboard again with `WAYLAND_DISPLAY` environment variable set like this:

```
WAYLAND_DISPLAY="$DISPLAY_IDENTIFIER" $BUILD/virtboard
```

Dbus interface
--------------

The keyboard will normally show and hide when the application supports input methods, but it's possible to hide and show it using Dbus. It supports 2 methods and 1 property.

To call the methods:

```
busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b true
busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 GetVisible
```

To check the property:

```
busctl --user get-property sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 Visible
```

To listen to property changes:

```
busctl --user monitor --match path=/sm/puri/OSK0
```

Third-party code
----------------

The files `shared/libgwater_wayland.c` and `shared/libgwater_wayland.h` come from the [libgwater](https://github.com/sardemff7/libgwater/tree/master/wayland) project. They should be never modified apart from updating. They should be updated together, and the commit message should specify the source libgwater revision.
